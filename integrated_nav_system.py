import numpy as np

from navigation_system import heights_map
from static_estimation import KalmanFilter


class IntegratedNaviSystem(object):

    def __init__(self):
        self.navi_systems = {}
        self.measurements = None
        self.last_estimation = None

        self.measurements_names = []
        self.estimation_names = []

    def add(self, system, name):
        self.navi_systems[name] = system
        self.measurements_names += system.vec_names

    def prepare(self, t_end):
        for system in self.navi_systems.values():
            system.prepare(t_end)

    def estimate(self, time, state_vector):
        estimation_vector = list()
        for system in self.navi_systems.values():
            estimation = system.estimate(time, state_vector)
            estimation_vector.append(estimation)

        # horizontally stack arrays
        self.measurements = np.hstack(estimation_vector)
        return self.measurements

    @property
    def estimation_size(self):
        return len(self.estimation_names)


class NaviBox(IntegratedNaviSystem):
    """
    Concrete realization
    """

    def __init__(self, estimator_params):
        super().__init__()

        self.estimation_names = ['y_gnss*']

        self.static_estimator = KalmanFilter(**estimator_params)

        self.history = {
            't': list(),
            'y~': list(),
            'r': list()
        }

    def estimate(self, time, state_vector):
        super().estimate(time, state_vector)

        # реализация БПОИ
        y_gnss = self.navi_systems['GNSS'].last_estimation[1]
        h_rba = self.navi_systems['RBA'].last_estimation

        x = state_vector[0]
        y_rba = h_rba + heights_map(x)

        r = y_gnss - y_rba

        self.static_estimator.eval(time, r)

        self.last_estimation = y_gnss - self.static_estimator.estimation[0]

        self.history['t'].append(time)
        self.history['y~'].append(self.last_estimation.A)
        self.history['r'].append(r)

        return self.last_estimation
