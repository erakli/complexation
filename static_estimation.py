from numpy import dot, matrix, eye, zeros_like


class StaticEstimation(object):

    def __init__(self):
        pass

    def eval(self, time, measurements):
        raise NotImplementedError


class KalmanFilter(StaticEstimation):

    def __init__(self, X_0, P_0, A, H, D_eta, D_ksi, W, V):
        super().__init__()

        self.prev_time = 0

        self.posterior_estimate = matrix(X_0)
        self.posterior_covariance = matrix(P_0)

        # predict
        self.prior_estimate = None
        self.prior_covariance = None

        self.measurement_matrix = matrix(H)

        self.measurement_noise_effect = matrix(V)
        self.measurement_noise_covariance = matrix(D_eta)

        self.process_noise_effect = matrix(W)
        self.process_noise_intensity = matrix(D_ksi)

        self.model_coeffs = matrix(A)

        self.history = {
            't': list(),
            'X^': list(),
            'P^': list(),
            'K': list(),
            'X*': list(),
            'P*': list()
        }

    def eval(self, time, measurements):
        self.predict(time)
        self.correct(measurements)

        self.prev_time = time

        self.history['t'].append(self.prev_time)

    def predict(self, time):
        """
        Prediction on next step, i+1

        :return:
        """
        F = self.fundamental_matrix(self.prev_time, time)
        X = self.posterior_estimate
        P = self.posterior_covariance
        W = self.process_noise_effect
        D_ksi = self.process_noise_intensity

        self.prior_estimate = F * X
        self.prior_covariance = F * P * F.T + W * D_ksi * W.T

        self.history['X^'].append(self.prior_estimate.A)
        self.history['P^'].append(self.prior_covariance.A)

    def correct(self, measurements):
        """
        Estimation on current step, i

        :param measurements:
        :return:
        """
        H = self.measurement_matrix
        X_hat = self.prior_estimate
        P_hat = self.prior_covariance
        D_eta = self.measurement_noise_covariance
        V = self.measurement_noise_effect

        Y = measurements

        # P = self.posterior_covariance

        # self.posterior_covariance = (P_hat.I + H.T * D_eta.I * H).I
        # self.posterior_estimate = X_hat + P * H.T * D_eta.I * (Y - H * X_hat)

        # filter gain
        K = P_hat * H.T * (H * P_hat * H.T + V * D_eta * V.T).I

        self.posterior_estimate = X_hat + K * (Y - H * X_hat)
        self.posterior_covariance = (eye(K.shape[1]) - K * H) * P_hat

        self.history['K'].append(K.A)
        self.history['X*'].append(self.posterior_estimate.A)
        self.history['P*'].append(self.posterior_covariance.A)

    def fundamental_matrix(self, t_prev, t):
        A = self.model_coeffs
        E = eye(A.shape[0])

        return E + A * (t - t_prev)

    @property
    def estimation(self):
        return self.posterior_estimate
