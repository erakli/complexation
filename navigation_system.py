import numpy as np

from error import SystematicError, FirstOrderShapingFilter


def heights_map(x):
    return 0


class NavigationSystem(object):

    def __init__(self, work_frequency=1):
        self.rand_error = None
        self.sys_error = None

        self.work_frequency = work_frequency

        self.vec_names = []

        self.last_estimation = None

    def init_errors(self, error_params):
        raise NotImplementedError

    def prepare(self, t_end):
        if self.rand_error is not None:
            self.rand_error.prepare(t_end,)  # discret_interval=1 / self.work_frequency)

    def error(self, time):
        error = np.zeros(self.size)
        if self.rand_error is not None:
            error += self.rand_error.get_value(time)
        if self.sys_error is not None:
            error += self.sys_error.get_value(time)
        return error

    def estimate(self, time, state_vector):
        raise NotImplementedError

    @property
    def size(self):
        return len(self.vec_names)


class GNSSReciever(NavigationSystem):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.vec_names = ['x_gnss', 'y_gnss', 'Vx_gnss', 'Vy_gnss']

        self.history = {
            'y_gnss': list(),
            'delta_gnss': list()
        }

    def init_errors(self, error_params):
        self.rand_error = FirstOrderShapingFilter(**error_params['rand'])
        self.sys_error = SystematicError(value=error_params['sys'])

    def estimate(self, time, state_vector):
        error = self.error(time)
        self.last_estimation = state_vector + error

        self.history['y_gnss'].append(self.last_estimation[1])
        self.history['delta_gnss'].append(error[1])

        return self.last_estimation


class RadioBaroAltimeter(NavigationSystem):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.vec_names = ['h_rba']

        self.history = {
            'h_rba': list(),
            'delta_rba': list()
        }

    def init_errors(self, error_params):
        self.rand_error = FirstOrderShapingFilter(**error_params['rand'])
        self.sys_error = SystematicError(value=error_params['sys'])

    def estimate(self, time, state_vector):
        x, y, _, _ = state_vector
        real_height = y - heights_map(x)
        error = self.error(time)
        self.last_estimation = real_height + error

        self.history['h_rba'].append(self.last_estimation[0])
        self.history['delta_rba'].append(error[0])

        return self.last_estimation
