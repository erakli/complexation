import os
import json
import logging

import numpy as np
from numpy import sqrt, cos, sin, radians
import pandas as pd

import matplotlib as mpl

from dynamic_object import Vehicle
from navigation_system import *
from integrated_nav_system import *

# output for latex
mpl.use('pgf')

import matplotlib.pyplot as plt

plt.style.use('grayscale')


pgf_with_pdflatex = {
    'pgf.texsystem': 'pdflatex',
    'font.family': 'serif',  # use serif/main font for text elements
    'text.usetex': True,     # use inline math for ticks
    'pgf.rcfonts': False,    # don't setup fonts from rc parameters
    'pgf.preamble': [
        r'\usepackage[T1,T2A]{fontenc}',
        r'\usepackage[utf8]{inputenc}',
        r'\usepackage[english,russian]{babel}',
        r'\DeclareUnicodeCharacter{2212}{--}',
        r'\usepackage{amsmath}'
     ],
}
mpl.rcParams.update(pgf_with_pdflatex)


CONFIG_FILE = 'config.json'
OUTPUT_DIR = 'output'

os.makedirs(OUTPUT_DIR, exist_ok=True)

with open(CONFIG_FILE, 'r') as f:
    PARAMS = json.load(f)


logging.basicConfig(level=logging.INFO)


def save(fig, name):
    fig.savefig(f'{OUTPUT_DIR}/{name}.pdf')
    fig.savefig(f'{OUTPUT_DIR}/{name}.pgf')

    logging.info(f'Figure {name} saved')


class Experiment(object):

    def __init__(self):
        self.modelling_time = 0

        X = PARAMS['vehicle_initial_params']['mean']
        vehicle_mean = [
            X['x'],
            X['y'],
            X['V'] * cos(radians(X['k'])),
            X['V'] * sin(radians(X['k'])),
        ]

        P = PARAMS['vehicle_initial_params']['cov']
        vehicle_cov = np.array([
            P['sigma_xy'],
            P['sigma_xy'],
            P['sigma_VxVy'],
            P['sigma_VxVy']
        ])
        vehicle_cov = np.diag(vehicle_cov**2)

        vehicle_initial_conditions = np.random.multivariate_normal(
            mean=vehicle_mean,
            cov=vehicle_cov
        )

        self.dynamic_object = Vehicle(vehicle_initial_conditions)

        gnss_error_param = PARAMS['error_param']['gnss']

        gnss = GNSSReciever()
        gnss.init_errors(gnss_error_param)

        rba_error_param = PARAMS['error_param']['rba']
        rba_error_param['sys'] = np.random.normal(
            rba_error_param['sys']['mean'],
            rba_error_param['sys']['std'])

        rba = RadioBaroAltimeter()
        rba.init_errors(rba_error_param)
        
        gnss_std = gnss_error_param['rand']['std']
        gnss_mu = gnss_error_param['rand']['corr_time']

        rba_std = rba_error_param['rand']['std']
        rba_mu = rba_error_param['rand']['corr_time']

        kf_params = {
            'X_0': [
                [0],  # delta_gnss
            ],
            'P_0': [
                4 * gnss_std ** 2
            ],
            'H': [
                1
            ],
            'A': [
                -gnss_mu
            ],

            # process noise
            'W': [
                1
            ],
            'D_ksi': [
                2 * gnss_std**2
            ],

            # measurements noise
            'V': [
                1
            ],
            'D_eta': [
                2 * rba_std**2
            ]
        }

        self.navi_box = NaviBox(kf_params)
        self.navi_box.add(gnss, name='GNSS')
        self.navi_box.add(rba, name='RBA')

    def run(self, modelling_time, control=None):
        logging.info(f'{type(self).__name__} started')

        if modelling_time is not None:
            self.modelling_time = modelling_time

        self.navi_box.prepare(self.modelling_time)

        for t in np.arange(start=1,
                           stop=self.modelling_time,
                           step=1.0):
            new_pos = self.dynamic_object.run(t, control)
            self.navi_box.estimate(t, new_pos)

        logging.info(f'{type(self).__name__} finished')


if __name__ == '__main__':
    control = np.array(PARAMS['control'])

    np.random.seed(0)

    experiment = Experiment()
    experiment.run(PARAMS['modelling_time'], control=control)

    _trajectory = {
        'x': [p[0] for p in experiment.dynamic_object.history['X']],
        'y': [p[1] for p in experiment.dynamic_object.history['X']]
    }

    _measurements = {

        'y_gnss': experiment.navi_box.navi_systems['GNSS'].history['y_gnss'],
        'h_rba': experiment.navi_box.navi_systems['RBA'].history['h_rba'],
    }

    _errors = {
        'delta_gnss':
            experiment.navi_box.navi_systems['GNSS'].history['delta_gnss'],
        'delta_rba':
            experiment.navi_box.navi_systems['RBA'].history['delta_rba'],
    }

    _predict = {
        'X_gnss^': [p[0][0] for p in
                    experiment.navi_box.static_estimator.history['X^']],
        'P_gnss^': [3 * sqrt(p[0][0]) for p in
                    experiment.navi_box.static_estimator.history['P^']],
        # 'X_rba^': [p[1][0] for p in
        #            experiment.navi_box.static_estimator.history['X^']],
        # 'P_rba^': [p[1][1] for p in
        #            experiment.navi_box.static_estimator.history['P^']],
    }

    _correction = {
        'X_gnss*': [p[0][0] for p in
                    experiment.navi_box.static_estimator.history['X*']],
        'P_gnss*': [p[0][0] for p in
                    experiment.navi_box.static_estimator.history['P*']],
        # 'X_rba*': [p[1][0] for p in
        #            experiment.navi_box.static_estimator.history['X*']],
        # 'P_rba*': [p[1][1] for p in
        #            experiment.navi_box.static_estimator.history['P*']],
    }

    _gain = {
        'K_gnss': [p[0][0] for p in
                   experiment.navi_box.static_estimator.history['K']],
        # 'K_rba': [p[1][0] for p in
        #           experiment.navi_box.static_estimator.history['K']],
    }

    _navi_box = {
        't': experiment.navi_box.history['t'],
        'y~': [p[0][0] for p in experiment.navi_box.history['y~']],
        'r': [p[0] for p in experiment.navi_box.history['r']]
    }

    trajectory = pd.DataFrame(_trajectory)
    measurements = pd.DataFrame(_measurements)
    errors = pd.DataFrame(_errors)
    predict = pd.DataFrame(_predict)
    correction = pd.DataFrame(_correction)
    gain = pd.DataFrame(_gain)
    navi_box = pd.DataFrame(_navi_box)

    nrows = [1] * 8  # [2, 2, 1, 2, 2]
    names = []

    pairs = [plt.subplots(nrows=n, sharex='col', figsize=(6, 4)) for n in nrows]
    figs = [pair[0] for pair in pairs]
    axes = np.hstack(tuple([pair[1] for pair in pairs])).tolist()

    default_linewidth = 0.7

    for ax in axes:
        kwd = {'color': 'black', 'linewidth': 1}
        ax.axhline(**kwd)
        # ax.axvline(**kwd)

    # axes[0].set_title('Траектория и измерения')  #############################
    # trajectory.plot(ax=axes[0], label='eee', linewidth=2, color='m')
    # measurements.plot(ax=axes[0])

    # TODO: change on true iteration
    ax_iter = axes.__iter__()

    ax = ax_iter.__next__()
    names.append('trajectory_and_measurements')

    ax.plot(navi_box['t'], trajectory['y'],
            label='$ y $',
            linewidth=1.2,
            # color='0.0',
            linestyle='-.')
    ax.plot(navi_box['t'], measurements['y_gnss'],
            label='$ y_{\\text{СНС}} $',
            linewidth=default_linewidth,
            color='0.3'
            )
    # сразу отображаем как y_rba, потому что у нас отсутствует профиль высот
    ax.plot(navi_box['t'], measurements['h_rba'],
            label='$ y_{\\text{РБВ}} $',
            linewidth=default_linewidth,
            color='0.7'
            )
    ax.set_xlabel('$ t $, c')
    ax.set_ylabel('$ y $, м')

    # axes[1].set_title('Ошибки')  #############################################
    # errors.plot(ax=axes[1])

    ax = ax_iter.__next__()
    names.append('errors')

    ax.plot(navi_box['t'], errors['delta_gnss'],
            label='$ \Delta y_{\\text{СНС}} $',
            linewidth=default_linewidth,
            color='0.1'
            )
    ax.plot(navi_box['t'], errors['delta_rba'],
            label='$ \Delta h_{\\text{РБВ}} $',
            linewidth=default_linewidth,
            color='0.5'
            )

    ax.set_xlabel('$ t $, c')
    ax.set_ylabel('м')

    # axes[2].set_title('Прогноз')  ############################################
    # predict.plot(ax=axes[2])

    # axes[3].set_title('Оценка')  #############################################
    # correction.plot(ax=axes[3])

    ax = ax_iter.__next__()
    names.append('estimate')

    ax.plot(correction['X_gnss*'],
            label='$ \delta_{\\text{СНС}}^* $',
            linewidth=default_linewidth)
    ax.plot(3 * sqrt(correction['P_gnss*']),
            label='$ 3 \sqrt{\mathbf{P}_{\\text{СНС}}^*} $',
            linewidth=default_linewidth)
    ax.plot(-3 * sqrt(correction['P_gnss*']),
            label='$ -3 \sqrt{\mathbf{P}_{\\text{СНС}}^*} $',
            linewidth=default_linewidth)

    ax.set_xlabel('$ i $, номер шага')
    ax.set_ylabel('м')

    # axes[4].set_title('Коэффициенты фильтра Калмана')  #######################
    # gain.plot(ax=axes[4])

    ax = ax_iter.__next__()
    names.append('kf_gain')

    ax.plot(gain['K_gnss'],
            label='$ \mathbf{K} $',
            linewidth=default_linewidth)

    ax.set_xlabel('$ i $, номер шага')
    # ax.set_ylabel('м')

    # axes[6].set_title('Выход навигационной системы')  ########################
    # navi_box.plot(y=['y~'], ax=axes[5])

    ax = ax_iter.__next__()
    names.append('out_coord')

    ax.plot(navi_box['t'], navi_box['y~'],
            label='$ \\tilde{y} $',
            linewidth=default_linewidth)

    ax.set_xlabel('$ t $, с')
    ax.set_ylabel('м')

    # axes[6].set_title('Измеряемая величина')  ################################
    # navi_box.plot(y=['r'], ax=axes[6])

    ax = ax_iter.__next__()
    names.append('measure_coord')

    ax.plot(navi_box['t'], navi_box['r'],
            label='$ r $',
            linewidth=default_linewidth)

    ax.set_xlabel('$ t $, с')
    ax.set_ylabel('м')

    # axes[7].set_title('Difference')  #########################################
    # (navi_box['y~'] - trajectory['y']).plot(ax=axes[7])

    ax = ax_iter.__next__()
    names.append('difference')

    ax.plot(navi_box['t'], (navi_box['y~'] - trajectory['y']),
            label='$ \\tilde{y} - y $',
            linewidth=default_linewidth,
            color='0.2')

    ax.set_xlabel('$ t $, с')
    ax.set_ylabel('м')

    # axes[8].set_title('Error and estimate')  #################################
    # error_and_correction = pd.DataFrame(data={**_errors, **_correction})
    # error_and_correction.plot(ax=axes[8], y=['delta_gnss', 'X_gnss*'])
    # # error_and_correction.plot(ax=axes[9], y=['delta_rba', 'X_rba*'])

    ax = ax_iter.__next__()
    names.append('error_and_estimate')

    ax.plot(navi_box['t'], errors['delta_gnss'],
            label='$ \Delta y_{\\text{СНС}} $',
            linewidth=default_linewidth,
            color='0.6'
            )
    ax.plot(navi_box['t'], correction['X_gnss*'],
            label='$ \delta_{\\text{СНС}}^* $',
            linewidth=default_linewidth,
            color='0.1'
            )

    ax.set_xlabel('$ t $, с')
    ax.set_ylabel('м')

    for ax in axes:
        ax.legend()
        ax.grid(color='0.9')

    # NOTE: for subplots
    # for fig in figs:
    #     fig.subplots_adjust(left=0.1, bottom=0.05,
    #                         right=0.95, top=0.95, hspace=0.25)

    for fig, name in zip(figs, names):
        save(fig, name)

    plt.show()
