import numpy as np

from dynamic_system import DynamicSystem


class DynamicObject(object):

    def run(self, time_moment, control):
        raise NotImplementedError


class Vehicle(DynamicSystem, DynamicObject):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.vec_names = ['x', 'y', 'Vx', 'Vy']
        self.control = None

    def right_part(self, t, x):
        x, y, Vx, Vy = x
        omega = self.control

        V = np.sqrt(Vx**2 + Vy**2)

        dx = Vx
        dy = Vy
        dVx = -V * omega * np.cos(omega * t)
        dVy = V * omega * np.cos(omega * t)

        return np.array([
            dx,
            dy,
            dVx,
            dVy
        ])

    def run(self, time_moment, control):
        self.control = control
        return self.move_to(time_moment)
