import logging

import numpy as np

import sdeint

# from dynamic_system import DynamicSystem


class Error(object):

    def get_value(self, time):
        raise NotImplementedError


class SystematicError(Error):

    def __init__(self, value, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.value = value

    def get_value(self, time):
        return self.value


# class WhiteNoise(Error):
#
#     def __init__(self, sample_time, *args, **kwargs):
#         super().__init__(*args, **kwargs)
#
#         self.last_time = 0.0
#
#         self.sample_time = sample_time
#         self.normal_distribution_std = 1 / self.sample_time
#
#     def get_value(self, time):
#         # NB: не уверен, что стоит делать так
#         t = int((time - self.last_time) // self.sample_time) + 2
#         samples = np.random.normal(scale=self.normal_distribution_std,
#                                    size=t)
#         self.last_time = time
#         return samples[-1]


class ShapingFilter(Error):

    def __init__(self, corr_time, std, discret_interval=None, *args, **kwargs):
        """

        :param corr_time:
        :param std:
        :param discret_interval: interval of discretization, sec
        :param args:
        :param kwargs:
        """
        super().__init__(*args, **kwargs)

        self.std = std
        self.corr_time = corr_time

        self.discret_interval = discret_interval

        # WN correlation interval
        # https://www.mathworks.com/help/simulink/slref/bandlimitedwhitenoise.html
        self.step_size = self.corr_time / 10

        self.process_realization = None

    def prepare(self, t_end, discret_interval=None):
        logging.info(f'{ShapingFilter.__name__} preparing')

        if discret_interval is not None:
            self.discret_interval = discret_interval
        if self.discret_interval is None:
            self.discret_interval = self.step_size

        tspan = np.arange(0, t_end, step=self.step_size)
        x0 = 0

        result = sdeint.itoEuler(self.f, self.g, x0, tspan)

        # numpy's slicing [start:stop:step] creates a view of the the original
        # data, so it's constant time.
        skip_step = int(self.discret_interval / self.step_size)
        self.process_realization = result[0::skip_step].copy()

        logging.info(f'{ShapingFilter.__name__} preparation done')

    def f(self, x, t):
        """
        Vector-valued function to define the deterministic part of the system

        :param x:
        :param t:
        :return:
        """
        raise NotImplementedError

    def g(self, x, t):
        """
        Matrix-valued function to define the noise coefficients of the system

        :param x:
        :param t:
        :return:
        """
        raise NotImplementedError

    def get_value(self, time):
        interval = int(time / self.discret_interval)
        return self.process_realization[interval]


class FirstOrderShapingFilter(ShapingFilter):

    def f(self, x, t):
        return -self.corr_time * x

    def g(self, x, t):
        return np.sqrt(2) * self.std
