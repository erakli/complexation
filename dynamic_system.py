from scipy.integrate import solve_ivp


class DynamicSystem(object):

    def __init__(self, initial_state=None, **kwargs):
        self.time = 0.0
        self.state_vector = initial_state
        self.vec_names = []

        self.integrator_options = {
            'method': kwargs.get('method') or 'RK45',
            # 'dense_output': False,
            # 'atol': 1e-6,
            'rtol': kwargs.get('rtol') or 1e-5
        }

        self.history = {'t': list(), 'X': list()}

    def right_part(self, t, x):
        raise NotImplementedError

    def move_to(self, t_end):
        t_span = (self.time, t_end)
        sol = solve_ivp(self.right_part,
                        t_span,
                        self.state_vector,
                        **self.integrator_options)
        self.time = sol.t[-1]
        self.state_vector = sol.y[:, -1]

        self.history['t'].append(self.time)
        self.history['X'].append(self.state_vector)

        return self.state_vector
